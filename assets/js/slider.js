"use strict";

class Slider {
    constructor(selector) {
        this.sliderElement = document.querySelector(selector);
        this.slides = this.sliderElement.querySelectorAll(".slide");
        this.currentSlide = 0;
        this.intervalId = null;
        this.initSlides();
        this.bindNavigationEvents();
        this.buildSliderButtons();
    }

    initSlides() {
        this.showSlide(this.currentSlide);
    }

    bindNavigationEvents() {
        document.querySelector('.prev-slide').addEventListener('click', () => this.prevSlide());
        document.querySelector('.next-slide').addEventListener('click', () => this.nextSlide());
        document.getElementById('toggle-slider').addEventListener('click', () => this.toggleSliderVisibility());
        document.getElementById('autoplay-control').addEventListener('click', () => this.toggleAutoPlay());
    }

    showSlide(index) {
        this.slides.forEach(slide => slide.classList.remove('active'));
        this.slides[index].classList.add('active');
    }

    nextSlide() {
        this.currentSlide = (this.currentSlide + 1) % this.slides.length;
        this.showSlide(this.currentSlide);
    }

    prevSlide() {
        this.currentSlide = (this.currentSlide - 1 + this.slides.length) % this.slides.length;
        this.showSlide(this.currentSlide);
    }

    toggleSliderVisibility() {
        const slider = this.sliderElement.style;
        slider.display = slider.display === 'none' ? 'block' : 'none';
    }

    toggleAutoPlay() {
        const control = document.getElementById('autoplay-control');
        if (this.intervalId) {
            clearInterval(this.intervalId);
            this.intervalId = null;
            control.textContent = 'Start';
        } else {
            this.intervalId = setInterval(() => this.nextSlide(), 3000);
            control.textContent = 'Stop';
        }
    }

    buildSliderButtons() {
        const buttonsContainer = document.getElementById('slider-buttons');
        this.slides.forEach((_, index) => {
            let button = document.createElement('button');
            button.textContent = `Slide ${index + 1}`;
            button.addEventListener('click', () => this.setSlide(index));
            buttonsContainer.appendChild(button);
        });
    }

    setSlide(index) {
        this.currentSlide = index;
        this.showSlide(this.currentSlide);
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const slider = new Slider("#slider");
});
